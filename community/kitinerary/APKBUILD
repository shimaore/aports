# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kitinerary
pkgver=21.12.2
pkgrel=0
# armhf blocked by qt5-qtdeclarative
# ppc64le FTBFS
arch="all !armhf !ppc64le"
url="https://kontact.kde.org/"
pkgdesc="Data model and extraction system for travel reservation information"
license="LGPL-2.0-or-later"
depends_dev="
	kcalendarcore-dev
	kcontacts-dev
	kmime-dev
	kpkpass-dev
	libphonenumber-dev
	zxing-cpp-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	ki18n-dev
	libxml2-dev
	poppler-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	shared-mime-info
	zlib-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kitinerary-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# jsonlddocumenttest, mergeutiltest, airportdbtest, pkpassextractortest,
	# postprocessortest, calendarhandlertest, extractortest and knowledgedbtest are broken
	local skipped_tests="("
	local tests="
		jsonlddocument
		mergeutil
		airportdb
		pkpassextractor
		postprocessor
		calendarhandler
		extractor
		knowledgedb
		"
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)test"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "$skipped_tests"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
cd04977f1c06e00377eb7b5095afffe7b9a1ea67bad2944e18683e4ce5cc9594056977875abc255d2be30e712dddb56cc48683176be911d57528bc87b675b46a  kitinerary-21.12.2.tar.xz
"
