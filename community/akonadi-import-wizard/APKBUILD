# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=akonadi-import-wizard
pkgver=21.12.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by kmailtransport -> libkgapi -> qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
pkgdesc="Import data from other mail clients to KMail"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	akonadi-dev>=$pkgver
	extra-cmake-modules
	kauth-dev
	kconfig-dev
	kcontacts-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kidentitymanagement-dev
	kio-dev
	kmailtransport-dev
	kwallet-dev
	libkdepim-dev
	mailcommon-dev
	messagelib-dev
	pimcommon-dev
	qt5-qtbase-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-import-wizard-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
3a5fb6ea5f714310500cb20b095455fcaacb8d1b8cde72d5d5dee176bee2752ed677468c0e51ce2e8fd98b514aa2a8e94f910c3109a9b0d2eebc9edf82e70e5e  akonadi-import-wizard-21.12.2.tar.xz
"
