# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdenlive
pkgver=21.12.2
pkgrel=0
# s390x and riscv64 blocked by polkit -> kxmlgui
# ppc64le mlt uses 64bit long double while libgcc uses 128bit long double
arch="all !armhf !s390x !ppc64le !riscv64"
url="https://kdenlive.org"
pkgdesc="An intuitive and powerful multi-track video editor, including most recent video technologies"
license="GPL-2.0-or-later"
depends="
	ffmpeg
	frei0r-plugins
	qt5-qtquickcontrols
	"
makedepends="
	extra-cmake-modules
	kdeclarative-dev
	kdoctools-dev
	kfilemetadata-dev
	knewstuff-dev
	knotifyconfig-dev
	kxmlgui-dev
	mlt-dev
	purpose-dev
	qt5-qtbase-dev
	qt5-qtmultimedia-dev
	qt5-qtnetworkauth-dev
	qt5-qtsvg-dev
	rttr-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdenlive-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # Broken

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9d136cd4010515cc5b43186812f22ff82d8e126bb4584ef283ab4d7e03ed644965bd87890bf0118ae49746cdfbd122840d9d43246878d6ed5d49493f595d2e3a  kdenlive-21.12.2.tar.xz
"
