# Contributor: k0r10n <k0r10n.dev@gmail.com>
# Contributor: Ivan Tham <pickfire@riseup.net>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=extra-cmake-modules
pkgver=5.91.0
pkgrel=0
pkgdesc="Extra CMake modules"
url="https://projects.kde.org/projects/kdesupport/extra-cmake-modules"
arch="noarch !armhf" # Blocked by qt5-qtdeclarative
license="BSD-3-Clause"
depends="cmake"
makedepends="py3-sphinx"
checkdepends="qt5-qtbase-dev qt5-qtquickcontrols2-dev"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/extra-cmake-modules-$pkgver.tar.xz"
subpackages="$pkgname-doc"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DSphinx_BUILD_EXECUTABLE=/usr/bin/sphinx-build
	cmake --build build
}

check() {
	cd build
	# Broken tests
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "(KDEFetchTranslations|KDEInstallDirsTest.relative_or_absolute_usr|KDEInstallDirsTest.relative_or_absolute_qt)"
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
5134ce41171db708dc31ead1cc05c6c9e21187a49e4ab0930790440c2183de4c07d7e7170ac1e32d98344167313a39a132a431801c7927e986821705f807c457  extra-cmake-modules-5.91.0.tar.xz
"
