# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=knights
pkgver=21.12.2
pkgrel=0
pkgdesc="Chess board by KDE with XBoard protocol support"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://www.kde.org/applications/games/knights/"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kconfigwidgets-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kio-dev
	kplotting-dev
	ktextwidgets-dev
	kwallet-dev
	kxmlgui-dev
	libkdegames-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/knights-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
22a9bc3480144d1847d962c249610c17a8d2b37e4b25d76d3593b5ceeb3465de2faa257c7f33142ac5c7d879edc88b5ba5cee192df0ff94d4fcd0c24574a5e9a  knights-21.12.2.tar.xz
"
