# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=parley
pkgver=21.12.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kde.org/applications/education/org.kde.parley"
pkgdesc="Vocabulary Trainer"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	knewstuff-dev
	knotifications-dev
	kross-dev
	kxmlgui-dev
	libkeduvocdocument-dev
	libxml2-dev
	libxslt-dev
	qt5-qtbase-dev
	qt5-qtmultimedia-dev
	qt5-qtwebengine-dev
	sonnet-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/parley-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c238b1fb2fbdd90137299e08d36fde1af121f69359658675520d79e0df4fbd1c31bda7a66848839bf077de29ec64bf66bd533f1cf28e15f11ab6123c7de18d78  parley-21.12.2.tar.xz
"
