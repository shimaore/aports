# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kcalc
pkgver=21.12.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kxmlgui
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/utilities/org.kde.kcalc"
pkgdesc="Scientific Calculator"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	gmp-dev
	kconfig-dev
	kconfigwidgets-dev
	kcrash-dev
	kdoctools-dev
	kguiaddons-dev
	ki18n-dev
	kinit-dev
	knotifications-dev
	kxmlgui-dev
	mpfr-dev
	qt5-qtbase-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kcalc-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c7f3d454cb42923c3ee5cee8def68907f25d7d0ce7eecdbb076adab8ebbed78ee14aa6eb339aa965d7d6bfbb2ead2123769d1341d43d8a279284203d541bd6b9  kcalc-21.12.2.tar.xz
"
