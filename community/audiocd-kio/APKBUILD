# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=audiocd-kio
pkgver=21.12.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://www.kde.org/applications/multimedia/"
pkgdesc="Kioslave for accessing audio CDs"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	cdparanoia-dev
	extra-cmake-modules
	flac-dev
	kcmutils-dev
	kconfig-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	libkcddb-dev
	libkcompactdisc-dev
	libvorbis-dev
	qt5-qtbase-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/audiocd-kio-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7cdb93a30dba0b5f01c4fa9a97c681f07d72216adad58d158e00a524d9a648f37ec1c58448078e97df6c43a3f423e969b6dd262e0dc27fe1ba12a2626353f930  audiocd-kio-21.12.2.tar.xz
"
