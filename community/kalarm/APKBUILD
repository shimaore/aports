# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kalarm
pkgver=21.12.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
pkgdesc="Personal alarm scheduler"
license="GPL-2.0-or-later AND GFDL-1.2-only"
depends="kdepim-runtime"
makedepends="
	akonadi-contacts-dev
	akonadi-dev
	akonadi-mime-dev
	extra-cmake-modules
	kalarmcal-dev
	kauth-dev
	kcalendarcore-dev
	kcalutils-dev
	kcmutils-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kdbusaddons-dev
	kdelibs4support-dev
	kdoctools-dev
	kglobalaccel-dev
	kguiaddons-dev
	kholidays-dev
	ki18n-dev
	kiconthemes-dev
	kidentitymanagement-dev
	kidletime-dev
	kimap-dev
	kio-dev
	kjobwidgets-dev
	kmailtransport-dev
	kmime-dev
	knotifications-dev
	knotifyconfig-dev
	kpimtextedit-dev
	kservice-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libkdepim-dev
	libxslt-dev
	mailcommon-dev
	phonon-dev
	pimcommon-dev
	qgpgme
	qt5-qtbase-dev
	qt5-qtx11extras-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kalarm-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ab528a0f335320375635943525e18e3fbf3833a62b80e96a5fe56ccb5f24d90d57e0b2b81058f20c57567ca0772873b4801c3dccb306d76dc6dd83cdf1347631  kalarm-21.12.2.tar.xz
"
