# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=khelpcenter
pkgver=21.12.2
pkgrel=0
pkgdesc="Application to show KDE Applications' documentation"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> khtml
arch="all !armhf !s390x !riscv64"
url="https://userbase.kde.org/KHelpCenter"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	grantlee-dev
	karchive-dev
	kbookmarks-dev
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdoctools-dev
	khtml-dev
	ki18n-dev
	kinit-dev
	kservice-dev
	kwindowsystem-dev
	libxml2-dev
	qt5-qtbase-dev
	xapian-core-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/khelpcenter-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
75ad917f872df7dc8b85c7bfae51ae89e07cdf532c847a6191c2a94bf27e2384a29b06eb845b41f55e9bc9b344bc214456589b14c49d15e9a4457a7941c8f720  khelpcenter-21.12.2.tar.xz
"
