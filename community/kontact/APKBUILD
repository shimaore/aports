# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kontact
pkgver=21.12.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
# riscv64 disabled due to missing rust in recursive dependency
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
pkgdesc="Container application to unify several major PIM applications within one application"
license="GPL-2.0-or-later"
makedepends="
	akonadi-dev
	extra-cmake-modules
	grantleetheme-dev
	kcmutils-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kiconthemes-dev
	kontactinterface-dev
	kpimtextedit-dev
	kwindowsystem-dev
	libkdepim-dev
	pimcommon-dev
	qt5-qtbase-dev
	qt5-qtwebengine-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kontact-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
23c4ef9f64a3c69fa705e1bff89780ed665a0d950ac1dd1ff5729f88fe6c42a1d697b1f35d8edd68f0aea2ba6b0db46ff79612a28725d2994de423e31cdafae4  kontact-21.12.2.tar.xz
"
